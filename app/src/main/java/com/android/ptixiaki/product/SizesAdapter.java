package com.android.ptixiaki.product;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.ptixiaki.R;
import com.android.ptixiaki.product.model.Size;

import java.util.List;

/**
 * Created by angelogkiata on 10/12/2017.
 */

public class SizesAdapter extends RecyclerView.Adapter<SizesAdapter.SizeViewHolder> {

    private List<Size> sizes;
    private ProductSelectionInterface selectionInterface;
    private Size selectedSize;

    public SizesAdapter(List<Size> sizes, ProductSelectionInterface selectionInterface) {
        this.sizes = sizes;
        this.selectionInterface = selectionInterface;
    }

    @Override
    public SizeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SizeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.size_item_layout, parent, false), selectionInterface);
    }

    @Override
    public void onBindViewHolder(SizeViewHolder holder, int position) {
        final Size item = sizes.get(position);
        holder.setItem(item);
    }

    @Override
    public int getItemCount() {
        return sizes == null ? 0 : sizes.size();
    }

    class SizeViewHolder extends RecyclerView.ViewHolder {
        public final TextView sizeTextView;
        public final View offView;
        public final ProductSelectionInterface selectionInterface;

        public SizeViewHolder(View itemView, ProductSelectionInterface selectionInterface) {
            super(itemView);
            this.sizeTextView = (TextView) itemView.findViewById(R.id.size_text_view);
            this.selectionInterface = selectionInterface;
            this.offView = itemView.findViewById(R.id.size_disable);
        }

        public void setItem(final Size item) {
            this.sizeTextView.setText(item.getName());
            this.sizeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectionInterface.sizeSelected(item);
                    selectedSize = item;
                    notifyDataSetChanged();
                }
            });
            int selectedColor = itemView.getContext().getColor(android.R.color.black);
            int color = itemView.getContext().getColor(R.color.drawer_grey);
            this.sizeTextView.setTextColor(item == selectedSize ? selectedColor : color);
            this.sizeTextView.setTextSize(item == selectedSize ? 13 : 11);

            this.offView.setVisibility(!item.isInStock() ? View.VISIBLE : View.GONE);
        }
    }

    interface ProductSelectionInterface {
        void sizeSelected(Size item);
    }
}

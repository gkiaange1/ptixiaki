package com.android.ptixiaki.network;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by angelos on 07/10/2014.
 */
public class AGRequest<T> extends JsonRequest<T> {

    protected Class<T> responseType;

    /**
     * Creates a new request.
     *
     * @param method        the HTTP method to use
     * @param url           URL to fetch the JSON from
     * @param requestData   A {@link Object} to post and convert into json as the request. Null is allowed and indicates no parameters will be posted along with request.
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public AGRequest(int method, String url, Object requestData, Class<T> responseType, Listener<T> listener, ErrorListener errorListener) {
        super(method, url, (requestData == null) ? null : requestData instanceof JSONObject ? requestData.toString() : Mapper.string(requestData), listener, errorListener);
        this.responseType = responseType;
        setRetryPolicy(new DefaultRetryPolicy(25000, 0, 0));
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }

        if (responseType == null) {
            return (Response<T>) Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
        } else {
            try {
                return Response.success(Mapper.objectOrThrow(parsed, responseType), HttpHeaderParser.parseCacheHeaders(response));
            } catch (Exception e) {
                return Response.error(new ParseError(e));
            }
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-agent", "Android");
        return headers;
    }


}

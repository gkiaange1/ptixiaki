package com.android.ptixiaki.base;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.ptixiaki.AGApplication;
import com.android.ptixiaki.R;
import com.android.ptixiaki.base.model.DrawerItem;
import com.android.ptixiaki.base.model.DrawerModel;
import com.android.ptixiaki.basket.BasketActivity;
import com.android.ptixiaki.basket.BasketManager;
import com.android.ptixiaki.categorytree.CategoryTreeActivity;
import com.android.ptixiaki.home.HomeScreenActivity;
import com.android.ptixiaki.wishlist.WishListActivity;
import com.franmontiel.attributionpresenter.AttributionPresenter;
import com.franmontiel.attributionpresenter.entities.Attribution;
import com.franmontiel.attributionpresenter.entities.License;
import com.google.gson.Gson;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;


public class AGActivity extends AppCompatActivity {

    public static final String TITLE = "TITLE";
    protected MenuItem bagItem;
    protected Toolbar toolbar;

    public static void setBadgeCount(LayerDrawable icon, int count) {
        BadgeDrawable badge;
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable();
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static int convertDIPToPixels(double dip) {
        return (int) ((dip * AGApplication.getContext().getResources().getDisplayMetrics().density) + 0.5);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDelegateContentView(R.layout.activity_base);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        bagItem = menu.findItem(R.id.bag);
        setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        getLayoutInflater().inflate(layoutResID, (ViewGroup) findViewById(R.id.content_frame), true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupDrawer(toolbar);
        setTitle(null);
    }

    protected void setDelegateContentView(int id) {
        getDelegate().setContentView(id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.wishlist: {
                Intent intent = new Intent(AGActivity.this, WishListActivity.class);
                startActivity(intent);
            }
            return true;
            case R.id.bag: {
                Intent intent = new Intent(AGActivity.this, BasketActivity.class);
                startActivity(intent);
            }
            return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    protected void setTitle(String title) {
        TextView titleTextView = (TextView) findViewById(R.id.toolbar_title);
        if (TextUtils.isEmpty(title)) {
            titleTextView.setText(getIntent().getStringExtra(TITLE));
            return;
        }
        if (titleTextView != null) {
            titleTextView.setText(title);
        }
    }

    private AccountHeader getHeader() {
        return new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .build();
    }

    public void setupDrawer(Toolbar toolbar) {
        IDrawerItem x[] = getDrawerItemsArray();
        Drawer drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(getHeader())
                .addDrawerItems(x)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .build();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, null, R.string.launch_logo_desc, R.string.launch_logo_desc) {
        };
        drawer.setActionBarDrawerToggle(toggle);
    }

    private IDrawerItem[] getDrawerItemsArray() {
        String jsonString = "[{\"type\":\"primary\",\"label\":\"Home\",\"action\":\"FIRECANNON::GOTO_HOME\",\"icon\":\"home\"},{\"type\":\"primary\",\"label\":\"Shop\",\"icon\":\"shop\"},{\"type\":\"divider\",\"label\":\"\",\"action\":\"\",\"icon\":\"\"},{\"type\":\"primary\",\"label\":\"Wishlist\",\"icon\":\"wishlist\"},{\"type\":\"divider\",\"label\":\"\",\"action\":\"\",\"icon\":\"\"},{\"type\":\"primary\",\"label\":\"about\",\"icon\":\"about\"}]";
        Gson gson = new Gson();
        ArrayList<DrawerItem> drawerItems = gson.fromJson(jsonString, DrawerModel.class);
        ArrayList<IDrawerItem> itemsToReturn = new ArrayList<>();
        if (drawerItems == null) {
            return new IDrawerItem[0];
        }
        for (int i = 0; i < drawerItems.size(); i++) {
            DrawerItem item = drawerItems.get(i);

            int icon = getIconID(item.getIcon());
            IDrawerItem drawerItem = null;
            switch (item.getType()) {
                case "primary":
                    drawerItem = new PrimaryDrawerItem()
                            .withIdentifier(i)
                            .withName(item.getLabel())
                            .withIcon(icon)
                            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                @Override
                                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                    switch ((String) drawerItem.getTag()) {
                                        case "home": {
                                            Intent intent = new Intent(AGActivity.this, HomeScreenActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                        break;
                                        case "shop": {
                                            Intent intent = new Intent(AGActivity.this, CategoryTreeActivity.class);
                                            startActivity(intent);
                                        }
                                        break;
                                        case "wishlist": {
                                            Intent intent = new Intent(AGActivity.this, WishListActivity.class);
                                            startActivity(intent);
                                        }
                                        break;
                                        case "about": {
                                            new AttributionPresenter
                                                    .Builder(AGActivity.this)
                                                    .addAttributions(
                                                            new Attribution.Builder("AG Ptixiaki Egrasia")
                                                                    .addCopyrightNotice("Copyright 2018 Department of Informatics of T.E.I. of Athens")
                                                                    .addLicense(License.APACHE)
                                                                    .setWebsite("http://www.cs.teiath.gr/?lang=en")
                                                                    .build()
                                                    )
                                                    .addAttributions(getAttributions())
                                                    .build()
                                                    .showDialog("Shopping Application");
                                        }
                                        break;

                                    }
                                    return false;
                                }
                            })
                            .withSelectedBackgroundAnimated(false)
                            .withSelectedIconColorRes(R.color.drawer_grey)
                            .withIconTintingEnabled(true)
                            .withTextColorRes(R.color.drawer_grey)
                            .withSelectedTextColorRes(R.color.drawer_grey)
                            .withTag(item.getIcon());

                    break;
                case "divider":
                    drawerItem = new DividerDrawerItem();
                    break;
            }
            if (drawerItem != null) {
                itemsToReturn.add(drawerItem);
            }
        }
        return itemsToReturn.toArray(new IDrawerItem[itemsToReturn.size()]);
    }

    private int getIconID(String iconID) {
        switch (iconID) {
            case "home":
                return R.drawable.ic_home_black_24dp;
            case "shop":
                return R.drawable.ic_shop_black_24dp;
            case "wishlist":
                return R.drawable.ic_favorite_black_24dp;
            case "about":
                return R.drawable.ic_info_outline_black_24dp;

            default:
                return 0;
        }
    }

    protected void showBackArrow() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
    }

    protected void setBagCount(MenuItem item, int quantity) {
        if (item != null) {
            setBadgeCount((LayerDrawable) item.getIcon(), quantity);
        }
    }

    public Attribution[] getAttributions() {
        return new Attribution[]{
                new Attribution.Builder("Picasso")
                        .addCopyrightNotice("Copyright 2013 Square, Inc.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/square/picasso")
                        .build(),
                new Attribution.Builder("google-gson")
                        .addCopyrightNotice("Copyright 2008 Google Inc.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/google/gson")
                        .build(),
                new Attribution.Builder("Volley")
                        .addCopyrightNotice("Copyright 2008 Google Inc.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/google/volley")
                        .build(),
                new Attribution.Builder("Jackson")
                        .addCopyrightNotice("Copyright 2008 FasterXML.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/FasterXML/jackson")
                        .build(),
                new Attribution.Builder("OkHttp")
                        .addCopyrightNotice("Copyright Square.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/square/okhttp")
                        .build(),
                new Attribution.Builder("CircleIndicator")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/ongakuer/CircleIndicator")
                        .build(),
                new Attribution.Builder("MaterialDrawer")
                        .addCopyrightNotice("Copyright Mike Penz.")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/mikepenz/MaterialDrawer")
                        .build(),
                new Attribution.Builder("AttributionPresenter")
                        .addLicense(License.APACHE)
                        .setWebsite("https://github.com/franmontiel/AttributionPresenter")
                        .build()

        };
    }

    private static class BadgeDrawable extends Drawable {

        private int badgeColor;
        private int badgeStrokeColor;
        private float badgeStrokeSize;
        private Paint textPaint;
        private String count = "";
        private boolean mWillDraw = false;

        BadgeDrawable() {
            badgeStrokeSize = 3;

            float mTextSize = 32;
            textPaint = new Paint();
            textPaint.setColor(Color.BLACK);
            textPaint.setTextSize(mTextSize);
            textPaint.setAntiAlias(true);
            textPaint.setFakeBoldText(true);
            textPaint.setTextAlign(Paint.Align.CENTER);
            badgeColor = Color.WHITE;
            badgeStrokeColor = Color.BLACK;
        }

        @Override
        public void draw(Canvas canvas) {
            if (!mWillDraw) {
                return;
            }

            // Measurements for compositing the badge and the number on the
            // basket image
            // These measurements are arbitrary, and were set as a result of
            // trial-and-error

            // The width of the text
            float textWidth = textPaint.measureText(count);
            // the vertical spacing centerpoint for the text
            float width, height;
            width = height = convertDIPToPixels(30);

            float textVerticalSpace = Math.round(height - (height / 1.8));
            // Half the badge's height (effectively a 'radius' where rounded
            // corners are concerned)
            float badgeRadius = Math.round(height / 3.5);

            // Set up radii for the badge's rounded corners
            float outerRadii[] = {badgeRadius, badgeRadius, badgeRadius, badgeRadius, badgeRadius, badgeRadius, badgeRadius, badgeRadius};

            // Make a RoundRectShape using the badgeRadius to round the
            // corners
            RoundRectShape badgeRect = new RoundRectShape(outerRadii, null, null);

            // Set the height to twice the radius so the edges are
            // semi-circles
            float badgeHeight = badgeRadius * 2;
            // Set the width to be slightly wider than the number
            float badgeWidth = (float) (textWidth * 1.3);

            // If the width would make the badge narrower than a circle,
            // make it a circle
            if (badgeWidth < badgeRadius * 2) {
                badgeWidth = badgeRadius * 2;
            }

            // Resize the badge to our newly specified dimensions
            badgeRect.resize(badgeWidth, badgeHeight);

            // Create a ShapeDrawable from the badge (since RoundRectShape
            // can't be positioned)
            ShapeDrawable badge = new ShapeDrawable(badgeRect);

            // Set the new badge shape to always be in the upper-right of
            // the image
            // If the badge needs to grow for a longer number, it will grow
            // left
            int left = Math.round(width - badgeWidth);
            int top = 0;
            int right = Math.round(width);
            int bottom = Math.round(badgeHeight);

            // Set the parameters (position, colour) to the badge
            Rect badgeBounds = new Rect(left, top, right, bottom);
            badge.getPaint().setColor(badgeColor);
            badge.getPaint().setAntiAlias(true);
            badge.getPaint().setStyle(Paint.Style.FILL);

            badge.setBounds(badgeBounds);

            // Draw the badge first so it goes on the bottom
            badge.draw(canvas);
            badge.getPaint().setStrokeWidth(badgeStrokeSize);
            badge.getPaint().setColor(badgeStrokeColor);
            badge.getPaint().setStyle(Paint.Style.STROKE);

            badge.draw(canvas);

            // Draw the number on top of the badge
            canvas.drawText(count, width - (badgeWidth / 2), textVerticalSpace, textPaint);
        }

        /*
        Sets the count (i.e notifications) to display.
         */
        public void setCount(int count) {
            this.count = Integer.toString(count);

            // Only draw a badge if there are notifications.
            mWillDraw = count > 0;
            invalidateSelf();
        }

        @Override
        public void setAlpha(int alpha) {
            // do nothing
        }

        @Override
        public void setColorFilter(ColorFilter cf) {
            // do nothing
        }

        @Override
        public int getOpacity() {
            return PixelFormat.UNKNOWN;
        }
    }

}
package com.android.ptixiaki.productlist.model;

import com.android.ptixiaki.product.model.AltProduct;
import com.android.ptixiaki.product.model.Size;

import java.util.List;

/**
 * Created by angelogkiata on 18/11/2017.
 */

public interface ProductInterface {
    double getPrice();
    String getId();
    String getName();
    String getBrand();
    String getPromo();
    List<String> getImages();
    List<Size> getSizes();
    boolean isListProduct();
    float getRating();
    boolean inStock();
    String getInfoText();
    String getDeliveryText();
    String getSpectsText();
    List<AltProduct> getAlternativeProducts();
}

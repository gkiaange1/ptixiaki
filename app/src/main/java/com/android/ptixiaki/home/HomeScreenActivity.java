package com.android.ptixiaki.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.categorytree.CategoryTreeActivity;

/**
 * Created by angelos on 06/11/2014.
 */
public class HomeScreenActivity extends AGActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen_activity);
        findViewById(R.id.image_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeScreenActivity.this, CategoryTreeActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}


package com.android.ptixiaki.product.model;

import com.android.ptixiaki.productlist.model.ProductInterface;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ProductDetails implements ProductInterface {

    @JsonProperty("pid")
    public String pid;
    @JsonProperty("productCode")
    public String productCode;
    @JsonProperty("productName")
    public String productName;
    @JsonProperty("defaultColour")
    public String defaultColour;
    @JsonProperty("videoLink")
    public String videoLink;
    @JsonProperty("lozenge")
    public String lozenge;
    @JsonProperty("inStock")
    public boolean inStock;
    @JsonProperty("masterPid")
    public String masterPid;
    @JsonProperty("ratingCount")
    public String ratingCount;
    @JsonProperty("brandName")
    public String brandName;
    @JsonProperty("brandImg")
    public String brandImg;
    @JsonProperty("images")
    public List<String> images = null;
    @JsonProperty("isMax")
    public String isMax;
    @JsonProperty("isMin")
    public String isMin;
    @JsonProperty("wasMax")
    public String wasMax;
    @JsonProperty("wasMin")
    public String wasMin;
    @JsonProperty("savingText")
    public String savingText;
    @JsonProperty("promoText")
    public String promoText;
    @JsonProperty("lozengeString")
    public String lozengeString;
    @JsonProperty("offersHeight")
    public int offersHeight;
    @JsonProperty("wasText")
    public String wasText;
    @JsonProperty("stockText")
    public String stockText;
    @JsonProperty("formatted_dropShipText")
    public String formattedDropShipText;
    @JsonProperty("dropShipText")
    public String dropShipText;
    @JsonProperty("altProductTitle")
    public String altProductTitle;
    @JsonProperty("altProducts")
    public List<AltProduct> altProducts = null;
    @JsonProperty("info")
    public String info;
    @JsonProperty("specs")
    public String specs;
    @JsonProperty("rating")
    public int rating;
    @JsonProperty("preOrder")
    public boolean preOrder;
    @JsonProperty("shipmentID")
    public String shipmentID;
    @JsonProperty("minimumDeliveryLeadTime")
    public int minimumDeliveryLeadTime;
    @JsonProperty("maximumDeliveryLeadTime")
    public int maximumDeliveryLeadTime;
    @JsonProperty("size")
    public List<Size> size = null;
    @JsonProperty("del")
    public String del;
    @JsonProperty("reviews")
    public String reviews;

    public ProductDetails() {
    }

    public String getPid() {
        return pid;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public String getDefaultColour() {
        return defaultColour;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public String getLozenge() {
        return lozenge;
    }

    public boolean isInStock() {
        return inStock;
    }

    public String getMasterPid() {
        return masterPid;
    }

    public String getRatingCount() {
        return ratingCount;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getBrandImg() {
        return brandImg;
    }

    public List<String> getImages() {
        return images;
    }

    public String getIsMax() {
        return isMax;
    }

    public String getIsMin() {
        return isMin;
    }

    public String getWasMax() {
        return wasMax;
    }

    public String getWasMin() {
        return wasMin;
    }

    public String getSavingText() {
        return savingText;
    }

    public String getPromoText() {
        return promoText;
    }

    public String getLozengeString() {
        return lozengeString;
    }

    public int getOffersHeight() {
        return offersHeight;
    }

    public String getWasText() {
        return wasText;
    }

    public String getStockText() {
        return stockText;
    }

    public String getFormattedDropShipText() {
        return formattedDropShipText;
    }

    public String getDropShipText() {
        return dropShipText;
    }

    public String getAltProductTitle() {
        return altProductTitle;
    }

    public List<AltProduct> getAltProducts() {
        return altProducts;
    }

    public String getInfo() {
        return info;
    }

    public String getSpecs() {
        return specs;
    }

    public float getRating() {
        return rating;
    }

    public boolean isPreOrder() {
        return preOrder;
    }

    public String getShipmentID() {
        return shipmentID;
    }

    public int getMinimumDeliveryLeadTime() {
        return minimumDeliveryLeadTime;
    }

    public int getMaximumDeliveryLeadTime() {
        return maximumDeliveryLeadTime;
    }

    public List<Size> getSize() {
        return size;
    }

    public String getDel() {
        return del;
    }

    public String getReviews() {
        return reviews;
    }

    @Override
    public double getPrice() {
        return Double.valueOf(getIsMin());
    }

    @Override
    public String getId() {
        return getPid();
    }

    @Override
    public String getName() {
        return getProductName();
    }

    @Override
    public String getBrand() {
        return brandName;
    }

    @Override
    public String getPromo() {
        return getPromoText();
    }

    @Override
    public boolean isListProduct() {
        return false;
    }

    @Override
    public boolean inStock() {
        return inStock;
    }

    @Override
    public List<Size> getSizes() {
        return getSize();
    }

    @Override
    public String getInfoText() {
        return getInfo();
    }

    @Override
    public String getDeliveryText() {
        return getDel();
    }

    @Override
    public String getSpectsText() {
        return getSpecs();
    }

    @Override
    public List<AltProduct> getAlternativeProducts() {
        return getAltProducts();
    }
}

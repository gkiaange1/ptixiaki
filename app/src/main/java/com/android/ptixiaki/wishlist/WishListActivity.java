package com.android.ptixiaki.wishlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.productlist.model.ListProduct;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by angelos on 1/10/2015.
 */
public class WishListActivity extends AGActivity {

    View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wishlist_activity);
        ListView listView = (ListView) findViewById(R.id.list);
        emptyView = findViewById(R.id.empty_layout);
        if (WishListManager.INSTANCE.getProductList().getProducts().size() > 0) {
            initList(listView);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void initList(ListView listView) {
        WishListAdapter adapter = new WishListAdapter();
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bag, menu);
        return true;
    }

    private class WishListAdapter extends BaseAdapter {
        private ArrayList<ListProduct> products;

        WishListAdapter() {
            this.products = WishListManager.INSTANCE.getProductList().getProducts();
        }

        @Override
        public int getCount() {

            return (products == null) ? 0 : products.size();
        }

        @Override
        public ListProduct getItem(int position) {
            return products.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final ListProduct listProduct = getItem(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(WishListActivity.this).inflate(R.layout.wishlist_row, parent, false);
                viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
                viewHolder.productName = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.removeButton = convertView.findViewById(R.id.delete);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            String imageURL = "http://s7ondemand6.scene7.com//is/image/MothercareASE/{IMAGEID}?$wid=400&hei=400".replace("{IMAGEID}", listProduct.getImages().get(0));
            Picasso
                    .with(WishListActivity.this)
                    .load(imageURL)
                    .into(viewHolder.image);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Not implemented yet", Toast.LENGTH_SHORT).show();
                }
            });

            viewHolder.removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WishListManager.INSTANCE.removeProduct(listProduct);
                    notifyDataSetChanged();
                    if (WishListManager.INSTANCE.getProductList().getProducts().size() == 0)
                        emptyView.setVisibility(View.VISIBLE);
                }
            });

            viewHolder.productName.setText(listProduct.getName());
            return convertView;
        }

        class ViewHolder {
            ImageView image;
            TextView productName;
            View removeButton;
        }
    }
}

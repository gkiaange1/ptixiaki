package com.android.ptixiaki.productlist;

import com.android.ptixiaki.productlist.model.ListProduct;

import java.util.ArrayList;

/**
 * Created by angelogkiata on 22/10/2017.
 */

public class ProductListManager {
    private static final ProductListManager ourInstance = new ProductListManager();

    public static ProductListManager getInstance() {
        return ourInstance;
    }

    private ArrayList<ListProduct> products;

    private ProductListManager() {
    }

    public ArrayList<ListProduct> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ListProduct> products) {
        this.products = products;
    }
}

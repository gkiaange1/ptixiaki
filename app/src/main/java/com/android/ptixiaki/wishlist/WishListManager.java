package com.android.ptixiaki.wishlist;

import android.util.Log;

import com.android.ptixiaki.AGApplication;
import com.android.ptixiaki.productlist.model.ListProduct;
import com.android.ptixiaki.productlist.model.ProductList;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;

/**
 * Created by angelos on 1/10/2015.
 */
public enum WishListManager {
    INSTANCE;

    private ProductList productList;

    public ProductList getProductList() {
        if (productList == null)
            return loadProductFromDisk();
        else
            return productList;
    }


    public void addProduct(ListProduct product) {
        if (getIndexOfProduct(product) == -1) {
            getProductList().getProducts().add(0, product);
            if (getProductList().getProducts().size() > 25) {
                getProductList().getProducts().remove(getProductList().getProducts().size() - 1);
            }
        } else {
            getProductList().getProducts().remove(getIndexOfProduct(product));
            getProductList().getProducts().add(0, product);
        }

        saveWishListToDisk();
    }

    public void removeProduct(ListProduct listProduct) {
        Iterator<ListProduct> i = getProductList().getProducts().iterator();
        while (i.hasNext()) {
            if (listProduct.getProductId().equals(i.next().getProductId())) {
                i.remove();
                break;
            }
        }
        saveWishListToDisk();
    }

    public int getIndexOfProduct(ListProduct product) {
        return getProductList().getProducts().indexOf(product);
    }

    private ProductList loadProductFromDisk() {
        try {
            FileInputStream streamIn = new FileInputStream(AGApplication.getContext().getFilesDir() + "/wishlist");
            ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
            productList = (ProductList) objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            Log.e("", "Failed loading wishList products from disk", e);
            e.printStackTrace();
        }
        if (productList == null) {
            productList = new ProductList();
        }
        return productList;
    }

    private void saveWishListToDisk() {
        try {
            FileOutputStream fout = new FileOutputStream(AGApplication.getContext().getFilesDir() + "/wishlist");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(productList);
            oos.close();
        } catch (IOException e) {
            Log.e("", "Failed to write wishList products to disk", e);
            e.printStackTrace();
        }
    }

    public boolean contains(ListProduct listProduct) {
        for (ListProduct product : getProductList().getProducts()) {
            if (listProduct.getProductId().equals(product.getProductId())) {
                return true;
            }
        }
        return false;
    }
}

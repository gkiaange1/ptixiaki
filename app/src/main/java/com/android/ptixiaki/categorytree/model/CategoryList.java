package com.android.ptixiaki.categorytree.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryList {

    private Shop shop;

    public Shop getShop() {
        return shop;
    }

    @JsonProperty("Shop")
    public void setShop(Shop shop) {
        this.shop = shop;
    }

   public static class Shop {

        private String id;

        public String getId() {
            return id;
        }

        @JsonProperty("ID")
        public void setId(String id) {
            this.id = id;
        }

        private ArrayList<Category> categories;

        public ArrayList<Category> getCategories() {
            return categories;
        }

        @JsonProperty("SubCats")
        public void setCategories(ArrayList<Category> categories) {
            this.categories = categories;
        }
    }
}
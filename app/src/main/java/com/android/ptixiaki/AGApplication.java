package com.android.ptixiaki;


import android.app.Application;
import android.content.Context;

import com.android.ptixiaki.network.volley.VolleyRequest;
import com.android.volley.toolbox.HurlStack;

public class AGApplication extends Application {
    private static final String TAG = AGApplication.class.getSimpleName();
    private static AGApplication instance;

    public void onCreate() {
        super.onCreate();
        instance = this;
        VolleyRequest.getInstance().initWithHurlStack(new HurlStack());
    }

    public static Context getContext() {
        return instance;
    }

    public static AGApplication getInstance() {
        return instance;
    }
}

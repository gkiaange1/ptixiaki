package com.android.ptixiaki.product;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.product.model.AltProduct;
import com.android.ptixiaki.productlist.ImagesPagerAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by angelogkiata on 28/12/2017.
 */

public class AlternativeProductAdapter extends RecyclerView.Adapter<AlternativeProductAdapter.ViewHolder> {
    private List<AltProduct> items;

    public AlternativeProductAdapter(List<AltProduct> items) {
        this.items = items;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.alt_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final AltProduct item = items.get(position);
        holder.setItem(item);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.image_view);

        }

        public void setItem(final AltProduct item) {
            String imageURL = ImagesPagerAdapter.getBaseUrl(ImagesPagerAdapter.Type.PLP).replace("{IMAGEID}", item.getImageID());
            Picasso.with(this.itemView.getContext())
                    .load(imageURL)
                    .into(image);
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), "This feature has not been implemented yet", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

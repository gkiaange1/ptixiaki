package com.android.ptixiaki.basket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by angelogkiata on 02/01/2018.
 */

public class PromoResponse {
    @JsonProperty("success")
    private Boolean success;
    @JsonProperty("promo")
    private String promo;
    @JsonProperty("amount")
    private Double amount;

    public PromoResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getPromo() {
        return promo;
    }

    public Double getAmount() {
        return amount;
    }
}

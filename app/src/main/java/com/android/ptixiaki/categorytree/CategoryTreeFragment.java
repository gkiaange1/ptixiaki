package com.android.ptixiaki.categorytree;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.ptixiaki.R;
import com.android.ptixiaki.categorytree.model.Category;
import com.android.ptixiaki.productlist.ProductListActivity;

import java.util.ArrayList;

import static com.android.ptixiaki.productlist.ProductListActivity.PRODUCT_LIST_ID;

/**
 * Created by angelos on 12/6/2014.
 */
public class CategoryTreeFragment extends Fragment {
    CategoryTreeFragmentAdapter adapter;
    ListView list;
    ArrayList<Category> categories;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            categories = (ArrayList<Category>) getArguments().getSerializable(CategoryTreeActivity.SUBCATS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View returnView = inflater.inflate(R.layout.category_tree_fragment, null);
        list = (ListView) returnView.findViewById(R.id.list);
        adapter = new CategoryTreeFragmentAdapter(categories);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItem(position).hasSubCats()) {
                    gotoSubCat(adapter.getItem(position).getSubCats());
                } else {
                    gotoProductList(adapter.getItem(position).getId());
                }
            }
        });
        return returnView;

    }

    private void gotoSubCat(ArrayList<Category> subCats) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CategoryTreeActivity.SUBCATS, subCats);
        CategoryTreeFragment categoryTreeFragment = new CategoryTreeFragment();
        categoryTreeFragment.setArguments(bundle);
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.category_container, categoryTreeFragment)
                .addToBackStack(null)
                .commit();
    }

    private void gotoProductList(String productListID) {
        Intent intent = new Intent(getActivity(), ProductListActivity.class);
        intent.putExtra(PRODUCT_LIST_ID, productListID);
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }


    public class CategoryTreeFragmentAdapter extends BaseAdapter {
        private ArrayList<Category> categoryList;

        CategoryTreeFragmentAdapter(ArrayList<Category> categories) {
            this.categoryList = categories;
            if (categories == null) {
                this.categoryList = CategoryTreeManager.getInstance().getRootCategory();
            }
        }

        @Override
        public int getCount() {
            return categoryList.size();
        }

        @Override
        public Category getItem(int i) {
            return (categoryList == null) ? new Category() : categoryList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_tree_row, viewGroup, false);
                viewHolder.categoryTitle = (TextView) convertView.findViewById(R.id.title);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.categoryTitle.setText(getItem(position).getName());
            return convertView;
        }

        class ViewHolder {
            TextView categoryTitle;
        }
    }
}
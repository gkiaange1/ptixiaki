package com.android.ptixiaki.network.volley;

/**
 * Created by angelos on 07/10/2014.
 */

import android.annotation.SuppressLint;

import com.android.ptixiaki.AGApplication;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import java.io.File;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class VolleyRequest {

    private static VolleyRequest mInstance = null;

    private static RequestQueue mRequestQueue = null;

    public static VolleyRequest getInstance() {
        if (mInstance == null)
            mInstance = new VolleyRequest();
        return mInstance;
    }

    @SuppressLint("NewApi")
    public void init() {
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        Network network = new BasicNetwork(new OkHttpStack(OKHttpManager.INSTANCE.getOkHttpClient()));
        mRequestQueue = new RequestQueue(new DiskBasedCache(new File(AGApplication.getContext().getCacheDir(), "volley")), network);
    }

    @SuppressLint("NewApi")
    public void initWithHurlStack(HurlStack stack) {
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        mRequestQueue = Volley.newRequestQueue(AGApplication.getContext(), stack);
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            throw new IllegalStateException("Not initialized");
        }
    }


    public void resetCookies() {
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    }

    public static RequestQueue newRequestQueue(int threadPoolSize) {
        ///Using OKHTTP
        Network network = new BasicNetwork(new OkHttpStack(OKHttpManager.INSTANCE.getOkHttpClient()));
        RequestQueue queue = new RequestQueue(new DiskBasedCache(new File(AGApplication.getContext().getCacheDir(), "volley")), network, threadPoolSize);
        queue.start();
        return queue;
    }

    public static  <T> Request<T> fire(Request<T> request){
      return   getInstance()
                .getRequestQueue()
                .add(request);
    }

}

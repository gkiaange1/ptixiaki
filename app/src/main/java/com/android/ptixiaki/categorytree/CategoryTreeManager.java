package com.android.ptixiaki.categorytree;

import com.android.ptixiaki.categorytree.model.Category;

import java.util.ArrayList;

/**
 * Created by angelos on 12/6/2014.
 */
public class CategoryTreeManager {
    private static final String TAG = CategoryTreeManager.class.getSimpleName();
    private static CategoryTreeManager mInstance = new CategoryTreeManager();
    private static ArrayList<Category> mCategoryArrayList;

    public void setCategoryList(ArrayList<Category> categoryList) {
        mCategoryArrayList = categoryList;
    }

    public static CategoryTreeManager getInstance() {
        return mInstance;
    }

    CategoryTreeManager() {

    }

    public ArrayList<Category> getRootCategory() {
        return mCategoryArrayList;
    }
    public Category findCategory(String id, ArrayList<Category> categories) {
        if (categories != null) {
            for (Category category : categories) {
                if (id.equalsIgnoreCase(category.getId()))
                    return category;

                if (category.getSubCats() != null && !category.getSubCats().isEmpty()) {
                    Category category2 = findCategory(id, category.getSubCats());
                    if (category2 != null)
                        return category2;
                }
            }
        }
        return null;
    }
}

package com.android.ptixiaki.productlist.model;

import com.android.ptixiaki.product.model.AltProduct;
import com.android.ptixiaki.product.model.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by angelos on 12/30/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListProduct implements Serializable, ProductInterface {

  private static final long serialVersionUID = -2906097441504120462L;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("rating")
    private int rating;
    @JsonProperty("isMin")
    private String isMin;
    @JsonProperty("wasMin")
    private String wasMin;
    @JsonProperty("price")
    private double price;
    @JsonProperty("lozengeString")
    private String lozengeString;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("images")
    private List<String> images = null;

    public ListProduct() {
    }

    public void  setSelf(ProductInterface productInterface) {
        this.productId = productInterface.getId();
        this.name = productInterface.getName();
        this.rating = (int) productInterface.getRating();
        this.price = productInterface.getPrice();
        this.lozengeString = productInterface.getPromo();
        this.images = productInterface.getImages();
    }

    public String getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public float getRating() {
        return rating;
    }

    public String getIsMin() {
        return isMin;
    }

    public String getWasMin() {
        return wasMin;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormated() {
        return String.format("£ %s", getPrice());
    }

    public String getLozengeString() {
        return lozengeString;
    }

    public List<String> getImages() {
        return images;
    }

    @Override
    public String getId() {
        return getProductId();
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public String getPromo() {
        return getLozengeString();
    }

    @Override
    public boolean isListProduct() {
        return true;
    }

    @Override
    public boolean inStock() {
        return true;
    }

    @Override
    public List<Size> getSizes() {
        return null;
    }

    @Override
    public String getInfoText() {
        return null;
    }

    @Override
    public String getDeliveryText() {
        return null;
    }

    @Override
    public String getSpectsText() {
        return null;
    }

    @Override
    public List<AltProduct> getAlternativeProducts() {
        return null;
    }
}
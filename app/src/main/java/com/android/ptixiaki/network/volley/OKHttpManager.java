package com.android.ptixiaki.network.volley;

import okhttp3.OkHttpClient;

public enum OKHttpManager {
    INSTANCE;
    private OkHttpClient okHttpClient;

    public OkHttpClient getOkHttpClient() {
        if (okHttpClient == null)
            okHttpClient = new OkHttpClient();
        return okHttpClient;
    }
}

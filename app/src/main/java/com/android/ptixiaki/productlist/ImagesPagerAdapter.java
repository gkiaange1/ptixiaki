package com.android.ptixiaki.productlist;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.ptixiaki.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImagesPagerAdapter extends PagerAdapter {
    private List<String> images;
    private String productId;
    private Type type;

    public enum Type {
        PLP, PDP
    }

    public ImagesPagerAdapter(String productId, List<String> images, Type type) {
        this.images = images;
        this.productId = productId;
        this.type = type;
    }

    @Override
    public int getCount() {
        return (images == null) ? 0 : images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        String imageURL = getBaseUrl(type).replace("{IMAGEID}", images.get(position));
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.product_image, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        final View progress = view.findViewById(R.id.progress);
        Picasso.with(container
                .getContext())
                .load(imageURL)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progress.setVisibility(View.GONE);
                    }
                });

        container.addView(view, 0);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public static String getBaseUrl(Type type) {
        switch (type) {
            case PLP:
                return "http://s7ondemand6.scene7.com//is/image/MothercareASE/{IMAGEID}?$wid=400&hei=400";
            case PDP:
                return "http://s7ondemand6.scene7.com//is/image/MothercareASE/{IMAGEID}?$wid=1000&hei=1000";
            default:
                return "http://s7ondemand6.scene7.com//is/image/MothercareASE/{IMAGEID}?$wid=400&hei=400";
        }
    }
}


package com.android.ptixiaki.categorytree;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.categorytree.model.CategoryList;
import com.android.ptixiaki.network.CachedAGRequest;
import com.android.ptixiaki.network.volley.VolleyRequest;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

public class CategoryTreeActivity extends AGActivity implements Response.Listener<CategoryList>, Response.ErrorListener {
    public static final String SUBCATS = "subcats";
    public static final String ERROR_DOWNLOADING_CATEGORY_TREE = "Error Downloading Category Tree";
    public static final String CAT_TREE_URL = "https://private-accef3-ptixiaki.apiary-mock.com/cattree";
    private static final String TAG = CategoryTreeActivity.class.getSimpleName();
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categegory_tree_activity);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        downloadCategoryList();
    }


    private void downloadCategoryList() {
        VolleyRequest.fire(new CachedAGRequest<>(
                Request.Method.GET,
                CAT_TREE_URL,
                null,
                CategoryList.class,
                this,
                this,
                123000)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(CategoryTreeActivity.this, ERROR_DOWNLOADING_CATEGORY_TREE, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onResponse(CategoryList categoryList) {
        progressBar.setVisibility(View.GONE);
        if (categoryList != null) {
            CategoryTreeManager.getInstance().setCategoryList(categoryList.getShop().getCategories());
            initList();
        }
    }

    private void initList() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CategoryTreeActivity.SUBCATS, CategoryTreeManager.getInstance().getRootCategory());
        CategoryTreeFragment categoryTreeFragment = new CategoryTreeFragment();
        categoryTreeFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.category_container, categoryTreeFragment)
                .commitAllowingStateLoss();
    }
}
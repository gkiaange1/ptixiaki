package com.android.ptixiaki.basket;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.basket.model.BasketItem;
import com.android.ptixiaki.basket.model.PromoResponse;
import com.android.ptixiaki.network.AGRequest;
import com.android.ptixiaki.network.volley.VolleyRequest;
import com.android.ptixiaki.productlist.ImagesPagerAdapter;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by angelogkiata on 29/12/2017.
 */

public class BasketActivity extends AGActivity {
    public static final String PROMO_CODE_NOT_VALID = "Promo code not valid ";
    View emptyView;
    View proceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basket_activity);
        ListView listView = (ListView) findViewById(R.id.list);
        emptyView = findViewById(R.id.empty_layout);
        if (BasketManager.getInstance().getBasketQuantity() > 0) {
            initList(listView);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        proceed = toolbar.findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Not implemented yet", Toast.LENGTH_SHORT).show();
            }
        });
        proceed.setVisibility(BasketManager.getInstance().getBasketQuantity() > 0 ? View.VISIBLE : View.GONE);

        final EditText promoEditext = (EditText) findViewById(R.id.promo);
        promoEditext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    applyPromo(v.getText().toString(), promoEditext);
                }
                return false;
            }
        });

        setSupportActionBar(toolbar);
        showBackArrow();
    }

    private void applyPromo(final String promo, final EditText promoEditext) {
        if (!TextUtils.isEmpty(promo)) {
            byte[] data = promo.getBytes(StandardCharsets.UTF_8);
            String base64Encoded = Base64.encodeToString(data, Base64.NO_WRAP);
            String url = "https://private-accef3-ptixiaki.apiary-mock.com/promo".replace("{CODE}", base64Encoded);
            VolleyRequest.fire(new AGRequest<>(
                    Request.Method.GET,
                    url,
                    null,
                    PromoResponse.class,
                    new Response.Listener<PromoResponse>() {
                        @Override
                        public void onResponse(PromoResponse response) {
                            if (response.getSuccess()) {
                                BasketManager.getInstance().addPromo(response.getAmount());
                                updateTotal();
                                promoEditext.setText(null);
                                promoEditext.clearFocus();
                                return;
                            }
                            Toast.makeText(BasketActivity.this, PROMO_CODE_NOT_VALID, Toast.LENGTH_SHORT).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            promoEditext.setText(null);
                            promoEditext.clearFocus();
                            Toast.makeText(BasketActivity.this, PROMO_CODE_NOT_VALID, Toast.LENGTH_SHORT).show();

                        }
                    }));
        }
    }

    private void initList(ListView listView) {
        BasketAdapter adapter = new BasketAdapter();
        listView.setAdapter(adapter);
        updateTotal();
    }


    private void updateTotal() {
        TextView total = (TextView) findViewById(R.id.total_value);
        TextView promo = (TextView) findViewById(R.id.promo_value);
        View promoLabel = findViewById(R.id.promo_label);
        String promoTotal = BasketManager.getInstance().getPromoTotal();
        if (TextUtils.isEmpty(promoTotal)) {
            promo.setVisibility(View.GONE);
            promoLabel.setVisibility(View.GONE);
        } else {
            promo.setText(promoTotal);
            promo.setVisibility(View.VISIBLE);
            promoLabel.setVisibility(View.VISIBLE);
        }
        total.setText(BasketManager.getInstance().getBasketTotal());
    }

    private class BasketAdapter extends BaseAdapter {
        private ArrayList<BasketItem> items;

        BasketAdapter() {
            this.items = BasketManager.getInstance().getItems();
        }

        @Override
        public int getCount() {

            return (items == null) ? 0 : items.size();
        }

        @Override
        public BasketItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final BasketItem item = getItem(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.basket_item, parent, false);
                viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
                viewHolder.productName = (TextView) convertView.findViewById(R.id.name);
                viewHolder.remove = (ImageView) convertView.findViewById(R.id.remove);
                viewHolder.sizeName = (TextView) convertView.findViewById(R.id.size);
                viewHolder.price = (TextView) convertView.findViewById(R.id.price);
                viewHolder.increase = (ImageView) convertView.findViewById(R.id.increase);
                viewHolder.decrease = (ImageView) convertView.findViewById(R.id.decrease);
                viewHolder.quantity = (TextView) convertView.findViewById(R.id.quantity);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            String imageURL = ImagesPagerAdapter.getBaseUrl(ImagesPagerAdapter.Type.PLP).replace("{IMAGEID}", item.getImageId());
            Picasso
                    .with(parent.getContext())
                    .load(imageURL)
                    .into(viewHolder.image);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO
                    Toast.makeText(v.getContext(), "Not implemented yet", Toast.LENGTH_SHORT).show();
                }
            });
            viewHolder.quantity.setText(String.valueOf(item.getQuantity()));
            viewHolder.remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BasketManager.getInstance().removeItem(item);
                    notifyDataSetChanged();
                    updateTotal();
                    setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
                    if (BasketManager.getInstance().getBasketQuantity() == 0)
                        emptyView.setVisibility(View.VISIBLE);
                    proceed.setVisibility(View.GONE);
                }
            });

            viewHolder.increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BasketManager.getInstance().increaseQuantity(item);
                    updateTotal();
                    setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
                    notifyDataSetChanged();
                }
            });

            viewHolder.decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BasketManager.getInstance().decreaseQuantity(item);
                    updateTotal();
                    notifyDataSetChanged();
                    setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
                    if (BasketManager.getInstance().getBasketQuantity() == 0)
                        emptyView.setVisibility(View.VISIBLE);
                    proceed.setVisibility(View.GONE);
                }
            });
            viewHolder.sizeName.setText(item.getSizeName());
            viewHolder.sizeName.setVisibility(!TextUtils.isEmpty(item.getSizeName()) ? View.VISIBLE : View.GONE);
            viewHolder.price.setText(new DecimalFormat("##,##0.00€").format(Double.valueOf(item.getPrice()) * item.getQuantity()));
            viewHolder.productName.setText(item.getName());
            return convertView;
        }

        class ViewHolder {
            ImageView image;
            TextView productName;
            ImageView remove;
            TextView sizeName;
            TextView price;
            ImageView increase;
            ImageView decrease;
            TextView quantity;
        }
    }
}

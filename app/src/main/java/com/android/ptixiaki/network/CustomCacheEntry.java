package com.android.ptixiaki.network;

import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;

import java.util.Map;

import static com.android.volley.toolbox.HttpHeaderParser.parseDateAsEpoch;

/**
 * Created by angelos on 07/10/2014.
 */
public class CustomCacheEntry {
    private static final String CACHE_KEY = "Cached-Time";

    /**
     * Extracts a {@link com.android.volley.Cache.Entry} from a {@link com.android.volley.NetworkResponse}.
     * Cache-control headers are ignored. CacheTime is passed in as a variable.
     * Logic: Use custom header entry Cached-Time to control whether or not to insert the custom cache header.
     * If the Cached-Time entry is no longer valid set the cache time to 0 to indicate that it needs to be re downloaded.
     *
     * @param response The network response to parse headers from
     * @return a cache entry for the given response, or null if the response is not cacheable.
     */
    public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response, long cacheTime) {
        long now = System.currentTimeMillis();

        Map<String, String> headers = response.headers;

        long serverDate = 0;
        String serverEtag;
        String headerValue;

        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = parseDateAsEpoch(headerValue);
        }

        serverEtag = headers.get("ETag");

        // The expiry time
        final long softExpire = now + cacheTime;

        // See if this response has been cached before.
        String cachedString = headers.get(CACHE_KEY);
        long cachedTime = 0;
        if (cachedString != null) {
            cachedTime = Long.parseLong(cachedString);
        }

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;

        // If the cache time is still valid then use the existing cache time.
        if (cachedTime > now) {
            entry.softTtl = cachedTime;
            entry.ttl = cachedTime;
        }
        // Otherwise put the expiry time in the header to be cached.
        else {
            headers.put(CACHE_KEY, String.valueOf(softExpire));
            entry.softTtl = softExpire;                     // This may need to be changed to allow the data to be re-downloaded.
            entry.ttl = softExpire;
        }
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        Log.d("CUSTOM_CACHE_ENTRY", "Expiry Time: " + entry.ttl);

        return entry;
    }
}

package com.android.ptixiaki.base.model;

/**
 * Created by angelogkiata on 21/10/2017.
 */

public class DrawerItem {
    private String type;
    private String label;
    private String icon;

    public DrawerItem() {
    }

    public String getType() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String getIcon() {
        return icon;
    }
}

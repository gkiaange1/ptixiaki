package com.android.ptixiaki.basket.model;

import java.io.Serializable;

/**
 * Created by angelogkiata on 29/12/2017.
 */

public class BasketItem implements Serializable {

    private String id;
    private String name;
    private String sizeName;
    private String imageId;
    private int quantity;
    private String price;

    public BasketItem(String id, String name, String sizeName, String imageId, int quantity, String price) {
        this.id = id;
        this.name = name;
        this.sizeName = sizeName;
        this.imageId = imageId;
        this.quantity = quantity;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSizeName() {
        return sizeName;
    }

    public String getImageId() {
        return imageId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

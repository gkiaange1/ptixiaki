package com.android.ptixiaki.product;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.basket.BasketManager;
import com.android.ptixiaki.basket.model.BasketItem;
import com.android.ptixiaki.network.CachedAGRequest;
import com.android.ptixiaki.network.volley.VolleyRequest;
import com.android.ptixiaki.product.model.ProductDetails;
import com.android.ptixiaki.product.model.Size;
import com.android.ptixiaki.productlist.ImagesPagerAdapter;
import com.android.ptixiaki.productlist.model.ListProduct;
import com.android.ptixiaki.productlist.model.ProductInterface;
import com.android.ptixiaki.web.WebViewActivity;
import com.android.ptixiaki.wishlist.WishListManager;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.DecimalFormat;

import me.relex.circleindicator.CircleIndicator;


public class ProductDetailsActivity extends AGActivity implements SizesAdapter.ProductSelectionInterface {
    public static final String LIST_PRODUCT = "ListProduct";
    public static final String ERROR_DOWNLOADING_PRODUCT = "Error downloading Product";
    public static final String PRODUCT_ID = "productID";
    private static final String TAG = ProductDetailsActivity.class.getSimpleName();
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ViewPager imagePager;
    private TextView atb;
    private LinearLayout infoContainer;
    private View sizeContainer;
    private View progressLayout;
    private RatingBar ratingBar;
    private TextView price;
    private TextView name;
    private TextView brand;
    private TextView promo;
    private CircleIndicator indicator;
    private FloatingActionButton wishlistButton;
    private RecyclerView sizeRecyclerView;


    private Size selectedSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDelegate().setContentView(R.layout.product_details_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imagePager = (ViewPager) findViewById(R.id.pager);
        atb = (TextView) findViewById(R.id.atb_textview);
        infoContainer = (LinearLayout) findViewById(R.id.info_container);
        sizeContainer = findViewById(R.id.size_container);
        ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        price = (TextView) findViewById(R.id.price);
        name = (TextView) findViewById(R.id.name);
        brand = (TextView) findViewById(R.id.brand);
        promo = (TextView) findViewById(R.id.promo);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        wishlistButton = (FloatingActionButton) findViewById(R.id.wishlist_button);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        progressLayout = findViewById(R.id.progress_layout);
        sizeRecyclerView = (RecyclerView) findViewById(R.id.sizes_recyclerView);
        infoContainer = (LinearLayout) findViewById(R.id.info_container);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ListProduct product = (ListProduct) getIntent().getSerializableExtra(LIST_PRODUCT);

        showBackArrow();

        if (product == null) {
            downloadProductDetails(getIntent().getStringExtra(PRODUCT_ID));
        } else {
            init(product);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void init(final ProductInterface productInterface) {
        ImagesPagerAdapter adapter = new ImagesPagerAdapter(productInterface.getId(), productInterface.getImages(), ImagesPagerAdapter.Type.PDP);
        imagePager.setAdapter(adapter);
        indicator.setViewPager(imagePager);
        brand.setText(productInterface.getBrand());
        if (!TextUtils.isEmpty(productInterface.getPromo())) {
            promo.setText(productInterface.getPromo());
            promo.setVisibility(View.VISIBLE);
        } else {
            promo.setVisibility(View.GONE);
        }
        name.setText(productInterface.getName());
        price.setText(new DecimalFormat("##,##0.00€").format(productInterface.getPrice()));
        final ListProduct product = new ListProduct();
        product.setSelf(productInterface);
        wishlistButton.setImageResource(WishListManager.INSTANCE.contains(product) ? R.drawable.ic_favorite_white_36dp : R.drawable.ic_favorite_border_white_36dp);
        wishlistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!WishListManager.INSTANCE.contains(product)) {
                    WishListManager.INSTANCE.addProduct(product);
                    wishlistButton.setImageResource(WishListManager.INSTANCE.contains(product) ? R.drawable.ic_favorite_white_36dp : R.drawable.ic_favorite_border_white_36dp);
                } else {
                    WishListManager.INSTANCE.removeProduct(product);
                    wishlistButton.setImageResource(WishListManager.INSTANCE.contains(product) ? R.drawable.ic_favorite_white_36dp : R.drawable.ic_favorite_border_white_36dp);
                }
            }
        });
        if (productInterface.getRating() > 0) {
            ratingBar.setRating(productInterface.getRating());
            ratingBar.setVisibility(View.VISIBLE);
        } else {
            ratingBar.setVisibility(View.GONE);
        }
        wishlistButton.setVisibility(View.VISIBLE);
        progressLayout.setVisibility(View.GONE);

        if (!productInterface.inStock()) {
            atb.setBackgroundColor(ContextCompat.getColor(this, R.color.drawer_grey));
        }

        View.OnClickListener addToBag = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productInterface.getSizes() != null && productInterface.getSizes().size() > 0) {
                    if (selectedSize != null) {
                        if (selectedSize.isInStock()) {
                            BasketItem item = new BasketItem(selectedSize.id, productInterface.getName(), selectedSize.getName(), productInterface.getImages().get(0), 1, selectedSize.isMin);
                            BasketManager.getInstance().addProduct(item);
                            Toast.makeText(ProductDetailsActivity.this, "Product added to bag successfully", Toast.LENGTH_SHORT).show();
                            setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
                        } else {
                            Toast.makeText(ProductDetailsActivity.this, "The selected size is out of stock", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProductDetailsActivity.this, "Please select a size first", Toast.LENGTH_SHORT).show();
                    }
                } else if (productInterface.inStock()) {
                    BasketItem item = new BasketItem(productInterface.getId(), productInterface.getName(), "", productInterface.getImages().get(0), 1, String.valueOf(productInterface.getPrice()));
                    BasketManager.getInstance().addProduct(item);
                    Toast.makeText(ProductDetailsActivity.this, "Product added to bag successfully", Toast.LENGTH_SHORT).show();
                    setBagCount(bagItem, BasketManager.getInstance().getBasketQuantity());
                } else {
                    Toast.makeText(ProductDetailsActivity.this, "The selected size is out of stock", Toast.LENGTH_SHORT).show();
                }
            }
        };

        atb.setOnClickListener(addToBag);

        if (productInterface.isListProduct()) {
            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) collapsingToolbarLayout.getLayoutParams();
            params.setScrollFlags(0);
            collapsingToolbarLayout.setLayoutParams(params);
            return;
        }
        sizeContainer.setVisibility(View.VISIBLE);
        SizesAdapter sizesAdapter = new SizesAdapter(productInterface.getSizes(), this);
        sizeRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        sizeRecyclerView.setAdapter(sizesAdapter);

        infoContainer.setVisibility(View.VISIBLE);
        final TextView infoTextView = (TextView) findViewById(R.id.info);
        infoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
                intent.putExtra(TITLE, infoTextView.getText().toString());
                intent.putExtra(WebViewActivity.HTML, productInterface.getInfoText());
                startActivity(intent);
            }
        });
        final TextView specs = (TextView) findViewById(R.id.specs);
        specs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
                intent.putExtra(TITLE, specs.getText().toString());
                intent.putExtra(WebViewActivity.HTML, productInterface.getSpectsText());
                startActivity(intent);
            }
        });
        final TextView delivery = (TextView) findViewById(R.id.delivery);
        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
                intent.putExtra(TITLE, delivery.getText().toString());
                intent.putExtra(WebViewActivity.HTML, productInterface.getDeliveryText());
                startActivity(intent);
            }
        });

        if (productInterface.getAlternativeProducts() != null && productInterface.getAlternativeProducts().size() > 0) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.alt_recyclerView);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            AlternativeProductAdapter productAdapter = new AlternativeProductAdapter(productInterface.getAlternativeProducts());
            recyclerView.setAdapter(productAdapter);
            findViewById(R.id.alt_textview).setVisibility(View.VISIBLE);
        }
    }

    public void downloadProductDetails(String productId) {
        String url = "https://private-accef3-ptixiaki.apiary-mock.com/productdetails";
        url = url.replace("{PRODID}", productId);

        VolleyRequest.fire(new CachedAGRequest<>(
                Request.Method.GET,
                url,
                null,
                ProductDetails.class,
                new Response.Listener<ProductDetails>() {
                    @Override
                    public void onResponse(ProductDetails response) {
                        progressLayout.setVisibility(View.GONE);
                        init(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressLayout.setVisibility(View.GONE);
                        Toast.makeText(ProductDetailsActivity.this, ERROR_DOWNLOADING_PRODUCT, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                300000
        )).setTag(TAG);


    }

    @Override
    public void sizeSelected(Size item) {
        selectedSize = item;
    }


}
package com.android.ptixiaki.network;

/**
 * Created by angelos on 07/10/2014.
 */
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

public class CachedAGRequest<T> extends AGRequest<T> {

    private long cacheTime;

    /**
     * Creates a new request.
     *
     * @param method        the HTTP method to use
     * @param url           URL to fetch the JSON from
     * @param requestData   A {@link Object} to post and convert into json as the request. Null is allowed and indicates no parameters will be posted along with request.
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     * @param cacheTime     Time to cache in JSON to disk in MILLISECONDS.
     */
    public CachedAGRequest(int method, String url, Object requestData, Class<T> responseType, Response.Listener<T> listener, Response.ErrorListener errorListener, long cacheTime) {
        super(method, url, requestData, responseType, listener, errorListener);
        this.cacheTime = cacheTime;
    }

    /**
     * Parses the network response using a custom cache header parser to allow us to manually inject cache-headers.
     *
     * @param response the network response returned from the network.
     * @return a response mapped to the required {@code java.lang.Class}
     */
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(Mapper.objectOrThrow(jsonString, responseType), CustomCacheEntry.parseIgnoreCacheHeaders(response, cacheTime));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }

}

package com.android.ptixiaki.categorytree.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by angelos on 29/09/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Serializable {

    public boolean subCatExpands;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("ID")
    private String id;
    private ArrayList<Category> subCats;

    public Category() {
        setName("");
        setId("");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getSubCatExpands() {
        return subCatExpands;
    }

    @JsonProperty("SubCatExpands")
    public void setSubCatExpands(boolean subCatExpands) {
        this.subCatExpands = subCatExpands;
    }

    public ArrayList<Category> getSubCats() {
        return subCats;
    }

    @JsonProperty("SubCats")
    public void setSubCats(ArrayList<Category> subCats) {
        this.subCats = subCats;
    }


    public boolean hasSubCats() {
        return subCats != null;
    }
}

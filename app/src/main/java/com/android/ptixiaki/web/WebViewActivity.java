package com.android.ptixiaki.web;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;

/**
 * Created by angelogkiata on 27/12/2017.
 */

public class WebViewActivity extends AGActivity {

    public static final String HTML = "HTML";
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webView = (WebView) findViewById(R.id.web_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        showBackArrow();
        loadContent();
    }

    void loadContent() {
        if (!TextUtils.isEmpty(getIntent().getStringExtra(HTML))) {
            webView.loadData(getIntent().getStringExtra(HTML), "text/html; charset=UTF-8", null);
        } else {
            Toast.makeText(this, "Error Loading Content", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}

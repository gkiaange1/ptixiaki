
package com.android.ptixiaki.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AltProduct {

    @JsonProperty("prodID")
    public String prodID;
    @JsonProperty("clickUrl")
    public String clickUrl;
    @JsonProperty("imageID")
    public String imageID;

    public AltProduct() {
    }

    public String getProdID() {
        return prodID;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public String getImageID() {
        return imageID;
    }
}

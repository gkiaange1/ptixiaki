
package com.android.ptixiaki.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Size {

    @JsonProperty("pid")
    public String pid;
    @JsonProperty("inStock")
    public boolean inStock;
    @JsonProperty("id")
    public String id;
    @JsonProperty("masterPid")
    public String masterPid;
    @JsonProperty("isMax")
    public String isMax;
    @JsonProperty("isMin")
    public String isMin;
    @JsonProperty("wasMax")
    public String wasMax;
    @JsonProperty("wasMin")
    public String wasMin;
    @JsonProperty("savingText")
    public String savingText;
    @JsonProperty("name")
    public String name;
    @JsonProperty("atbString")
    public String atbString;
    @JsonProperty("shipmentId")
    public String shipmentId;
    @JsonProperty("stockText")
    public String stockText;
    @JsonProperty("info")
    public String info;
    @JsonProperty("specs")
    public String specs;
    @JsonProperty("del")
    public String del;


    public Size() {
    }

    public String getPid() {
        return pid;
    }

    public boolean isInStock() {
        return inStock;
    }

    public String getId() {
        return id;
    }

    public String getMasterPid() {
        return masterPid;
    }

    public String getIsMax() {
        return isMax;
    }

    public String getIsMin() {
        return isMin;
    }

    public String getWasMax() {
        return wasMax;
    }

    public String getWasMin() {
        return wasMin;
    }

    public String getSavingText() {
        return savingText;
    }

    public String getName() {
        return name;
    }

    public String getAtbString() {
        return atbString;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public String getStockText() {
        return stockText;
    }

    public String getInfo() {
        return info;
    }

    public String getSpecs() {
        return specs;
    }

    public String getDel() {
        return del;
    }
}

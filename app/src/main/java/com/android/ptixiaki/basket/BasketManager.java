package com.android.ptixiaki.basket;

import android.util.Log;

import com.android.ptixiaki.AGApplication;
import com.android.ptixiaki.basket.model.BasketItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by angelogkiata on 29/12/2017.
 */

public class BasketManager {
    private static final BasketManager ourInstance = new BasketManager();
    private static final String BASKET = "/basket";

    public static BasketManager getInstance() {
        return ourInstance;
    }

    private ArrayList<BasketItem> items;
    private ArrayList<Double> promos;

    private BasketManager() {
        items = loadItemsFromDisk();
        promos = new ArrayList<>();
    }

    public ArrayList<BasketItem> getItems() {
        return items;
    }

    public void addProduct(BasketItem item) {
        for (BasketItem basketItem : items) {
            if (basketItem.getId().equals(item.getId())) {
                basketItem.setQuantity(basketItem.getQuantity() + item.getQuantity());
                saveItemsToDisk();
                return;
            }
        }
        items.add(item);
        saveItemsToDisk();
    }


    public void removeItem(BasketItem item) {
        Iterator<BasketItem> i = items.iterator();
        while (i.hasNext()) {
            if (item.getId().equals(i.next().getId())) {
                i.remove();
                break;
            }
        }
        saveItemsToDisk();
    }


    private ArrayList<BasketItem> loadItemsFromDisk() {
        try {
            FileInputStream streamIn = new FileInputStream(AGApplication.getContext().getFilesDir() + BASKET);
            ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
            items = (ArrayList<BasketItem>) objectinputstream.readObject();
            objectinputstream.close();
        } catch (Exception e) {
            Log.e("", "Failed loading items from disk", e);
            e.printStackTrace();
        }
        if (items == null) {
            items = new ArrayList<>();
        }
        return items;
    }

    private void saveItemsToDisk() {
        try {
            FileOutputStream fout = new FileOutputStream(AGApplication.getContext().getFilesDir() + BASKET);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(items);
            oos.close();
        } catch (IOException e) {
            Log.e("", "Failed to write items to disk", e);
            e.printStackTrace();
        }
    }

    public void increaseQuantity(BasketItem item) {
        for (BasketItem basketItem : items) {
            if (basketItem.getId().equals(item.getId())) {
                basketItem.setQuantity(basketItem.getQuantity() + 1);
                saveItemsToDisk();
                return;
            }
        }
    }

    public void decreaseQuantity(BasketItem item) {
        Iterator<BasketItem> i = items.iterator();
        while (i.hasNext()) {
            if (item.getId().equals(i.next().getId())) {
                item.setQuantity(item.getQuantity() - 1);
                if (item.getQuantity() < 1) {
                    i.remove();
                }
                saveItemsToDisk();
                break;
            }
        }
    }

    public int getBasketQuantity() {
        int count = 0;
        for (BasketItem basketItem : items) {
            count += basketItem.getQuantity();
        }
        return count;
    }

    public String getBasketTotal() {
        double total = 0;
        for (BasketItem basketItem : items) {
            total += (Double.valueOf(basketItem.getPrice()) * basketItem.getQuantity());
        }
        double promoTotal = 0;
        for (double item : promos) {
            promoTotal += item;
        }

        total = total - promoTotal;
        if (total < 0) {
            total = 0;
        }
        return new DecimalFormat("##,##0.00€").format(total);
    }

    public String getPromoTotal() {
        double total = 0;
        for (double item : promos) {
            total += item;
        }
        return total > 0 ? "- ".concat(new DecimalFormat("##,##0.00€").format(total)) : null;
    }

    public void addPromo(Double amount) {
        promos.add(amount);
    }
}

package com.android.ptixiaki.productlist.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by angelos on 12/30/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductList implements Serializable {

    private static final long serialVersionUID = -7962400809509955068L;
    @JsonProperty("Products")
    private ArrayList<ListProduct> products;



    @JsonProperty("Products")
    public void setProducts(ArrayList<ListProduct> paramArrayOfProductElement) {
        this.products = paramArrayOfProductElement;
    }

    @JsonProperty("Products")
    public ArrayList<ListProduct> getProducts() {
        return this.products;
    }

     public ProductList() {
        products = new ArrayList<ListProduct>();
    }
}
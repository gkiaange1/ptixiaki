package com.android.ptixiaki.productlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ptixiaki.R;
import com.android.ptixiaki.base.AGActivity;
import com.android.ptixiaki.network.CachedAGRequest;
import com.android.ptixiaki.network.volley.VolleyRequest;
import com.android.ptixiaki.product.ProductDetailsActivity;
import com.android.ptixiaki.productlist.model.ListProduct;
import com.android.ptixiaki.productlist.model.ProductList;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.DecimalFormat;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


public class ProductListActivity extends AGActivity {
    public static final String PLEASE_PROVIDE_A_PRODUCT_LIST_ID = "Please provide a product list id";
    public static final String ERROR_DOWNLOADING_PRODUCTS = "Error downloading Products";
    public static final String PRODUCT_LIST_ID = "ProductListId";
    private ProgressBar progressBar;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list_activity);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        list = (ListView) findViewById(R.id.list);
        String productListId = getIntent().getStringExtra(PRODUCT_LIST_ID);
        if (TextUtils.isEmpty(productListId)) {
            Toast.makeText(this, PLEASE_PROVIDE_A_PRODUCT_LIST_ID, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        String url = "https://private-accef3-ptixiaki.apiary-mock.com/productlist".replace("{CATID}", productListId);
        VolleyRequest.fire(new CachedAGRequest<>(
                Request.Method.GET,
                url,
                null,
                ProductList.class,
                new Response.Listener<ProductList>() {
                    @Override
                    public void onResponse(ProductList response) {
                        progressBar.setVisibility(View.GONE);
                        ProductListManager.getInstance().setProducts(response.getProducts());
                        displayProducts();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ProductListActivity.this, ERROR_DOWNLOADING_PRODUCTS, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                5000000));
    }

    private void displayProducts() {
        ProductListAdapter adapter = new ProductListAdapter(ProductListManager.getInstance().getProducts());
        list.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    class ProductListAdapter extends BaseAdapter {
        private ArrayList<ListProduct> products;

        ProductListAdapter(ArrayList<ListProduct> listProducts) {
            this.products = listProducts;
        }

        @Override
        public int getCount() {
            return (products == null) ? 0 : products.size();
        }

        @Override
        public ListProduct getItem(int position) {
            return products.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            final ListProduct listProduct = getItem(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false);
                viewHolder.imageViewPager = (ViewPager) convertView.findViewById(R.id.pager);
                viewHolder.productName = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.ratingBar = (RatingBar) convertView.findViewById(R.id.rating_bar);
                viewHolder.priceNowTextView = (TextView) convertView.findViewById(R.id.price);
                viewHolder.lozenge = (TextView) convertView.findViewById(R.id.lozenge);
                viewHolder.indicator = (CircleIndicator) convertView.findViewById(R.id.indicator);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            if (listProduct.getRating() > 0) {
                viewHolder.ratingBar.setRating(listProduct.getRating());
                viewHolder.ratingBar.setVisibility(View.VISIBLE);

            } else {
                viewHolder.ratingBar.setVisibility(View.GONE);
            }


            viewHolder.priceNowTextView.setText(new DecimalFormat("##,##0.00€").format(listProduct.getPrice()));
            viewHolder.productName.setText(listProduct.getName());
            viewHolder.imageViewPager.setOffscreenPageLimit(1);
            viewHolder.imageViewPager.setAdapter(new ImagesPagerAdapter(listProduct.getProductId(), listProduct.getImages(), ImagesPagerAdapter.Type.PLP));
            viewHolder.indicator.setViewPager(viewHolder.imageViewPager);
            if (!TextUtils.isEmpty(listProduct.getLozengeString())) {
                viewHolder.lozenge.setText(listProduct.getLozengeString());
                viewHolder.lozenge.setVisibility(View.VISIBLE);
            } else {
                viewHolder.lozenge.setVisibility(View.GONE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                    if (position == 0) {
                        String prodId = listProduct.getProductId();
                        intent.putExtra(ProductDetailsActivity.PRODUCT_ID, prodId);
                    } else {
                        intent.putExtra(ProductDetailsActivity.LIST_PRODUCT, listProduct);
                    }
                    startActivity(intent);
                }
            });

            return convertView;
        }

        class ViewHolder {
            ViewPager imageViewPager;
            TextView productName;
            RatingBar ratingBar;
            TextView priceNowTextView;
            TextView lozenge;
            CircleIndicator indicator;
        }
    }
}
